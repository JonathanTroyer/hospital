# AHTG tech interview project

This project uses [.NET Core](https://dotnet.microsoft.com) for a simple REST backend
and [Vue](https://vuejs.org) for a frontend.

## How to run

1. If you haven't already, [install .Net Core](https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/install)
2. [Download PostgreSQL](https://www.postgresql.org/download/) and install it
3. Run `db-init.sql` against your fresh instance of PostgreSQL
4. Set an environment variable with the password used during the installation
5. Update the configuration strings as needed in `backend/appsettings.json` and `backend/appsettings.Development.json` to match the username and password used in step 1
6. Open `backend` and run `dotnet restore`
7. Launch the backend
    * In VSCode, go to the debugger view and select Backend, then press F5 or the run icon
    * In Visual Studio, simply hit the run icon (it should say IISExpress)
8. Open `frontend` and run `npm i`
9. Launch the frontend by running `npm run serve`