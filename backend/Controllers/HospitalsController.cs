﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using backend.Settings;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Npgsql;

namespace backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HospitalsController : ControllerBase
    {
        private DatabaseSettings db;

        public HospitalsController(IOptions<DatabaseSettings> databaseSettings)
        {
            db = databaseSettings.Value;
        }

        internal IDbConnection Connection
        {
            get { return new NpgsqlConnection(db.ConnectionString); }
        }

        [HttpGet]
        public async Task<IActionResult> GetHospitals()
        {
            var sql = "SELECT * FROM hospitals";
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.QueryAsync<Hospital>(sql);
                return Ok(result.ToList());
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetHospital([FromRoute] [Required] int id)
        {
            var sql = "SELECT * FROM hospitals WHERE id = @id";
            using (var conn = Connection)
            {
                conn.Open();
                try
                {
                    var result = await conn.QuerySingleAsync<Hospital>(sql, new { id });
                    return Ok(result);
                }
                catch (InvalidOperationException)
                {
                    return NotFound();
                }
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddHospital([FromBody] [Required] Hospital newHospital)
        {
            var sql =
                @"INSERT INTO hospitals (name, description, location, beds_available, website) 
                    VALUES (@name, @description, @location, @bedsAvailable, @website)
                RETURNING id, name, description, location, beds_available, website;";
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.QuerySingleAsync<Hospital>(sql, new
                {
                    name = newHospital.Name,
                    description = newHospital.Description,
                    location = newHospital.Location,
                    bedsAvailable = newHospital.BedsAvailable,
                    website = newHospital.Website,
                });
                return CreatedAtAction(nameof(GetHospital), new { id = result.Id }, result);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditHospital([FromRoute] [Required] int id, [FromBody] [Required] Hospital updatedHospital)
        {
            var sql =
                @"UPDATE hospitals 
                    SET 
                        name = @name, 
                        description = @description, 
                        location = @location,
                        beds_available = @bedsAvailable,
                        website = @website
                    WHERE id = @id
                RETURNING id, name, description, location, beds_available, website;";
            using (var conn = Connection)
            {
                conn.Open();
                try
                {
                    var result = await conn.QuerySingleAsync<Hospital>(sql, new
                    {
                        id,
                        name = updatedHospital.Name,
                        description = updatedHospital.Description,
                        location = updatedHospital.Location,
                        bedsAvailable = updatedHospital.BedsAvailable,
                        website = updatedHospital.Website,
                    });
                    return Ok(result);
                }
                catch (InvalidOperationException)
                {
                    return NotFound();
                }
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHospital([FromRoute] [Required] int id)
        {
            var sql = "DELETE from hospitals WHERE id = @id";
            using (var conn = Connection)
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new { id });
                if (result > 0)
                {
                    return StatusCode(204);
                }
                else
                {
                    return NotFound();
                }
            }
        }
    }
}
