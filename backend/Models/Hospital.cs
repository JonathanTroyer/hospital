using System;

namespace backend
{
    public class Hospital
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public int BedsAvailable { get; set; }

        public string Website { get; set; }
    }
}
