using System;

namespace backend.Settings
{
    public class DatabaseSettings
    {
        private string connectionString;

        public string ConnectionString
        {
            get { return connectionString; }
            set { connectionString = value + $"Password={Environment.GetEnvironmentVariable("DATABASE_PASSWORD")};"; }
        }
    }
}
