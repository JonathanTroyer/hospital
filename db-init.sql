CREATE DATABASE hospital
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

USE hospital;

CREATE TABLE hospitals (
    id SERIAL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    "description" TEXT,
    "location" VARCHAR(255),
    beds_available INTEGER,
    website VARCHAR(255)
);

INSERT INTO hospitals ("name", "description", "location", beds_available, website) 
    VALUES (
        'Crystal Clinic', 
        'Crystal Clinic Orthopaedic Center is NE Ohio''s most respected group of orthopedic surgeons.',
        'Ohio',
        500,
        'https://www.crystalclinic.com/'
    )
