export const BASE_URL = 'https://localhost:5001'

export default class Api {
  /**
   * A simple method to wrap the Fetch API
    *
    * @static
    * @param {(object|string)} arg A string URL or an object with more arguments
    * @param arg.url The url to call
    * @param arg.args Arguments to pass to Fetch
    * @returns Promise
    * @memberof Api
    */
  static async call(arg) {
    if (typeof arg === 'string')
      arg = { url: arg };
    let { url, args = { method: 'GET' } } = arg;

    let formattedUrl = BASE_URL + url;

    if (args.body != null) {
      args.body = JSON.stringify(args.body);
    }
    args.headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };
    // eslint-disable-next-line no-console
    console.log(formattedUrl, args.method);
    let response = await fetch(formattedUrl, args);
    try {
      return await response.json();
    } catch(e) {
      // eslint-disable-next-line no-console
      console.info(e);
      return {};
    }
  }
}