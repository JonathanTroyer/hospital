import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Hospital from '../views/Hospital.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: { title: () => 'Hospitals | Home'}
  },
  {
    path: '/hospital/:id',
    name: 'hospital',
    component: Hospital,
    meta: { title: params => `Hospitals | ${params.hospital?.name ?? params.id}`}
  }
]

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title(to.params);
  next();
})

export default router
